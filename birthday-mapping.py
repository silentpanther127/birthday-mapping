import sys
from datetime import date

# Range
# We want the birthdays between October and January inclusive.
start = date(1, 10, 1)
end = date(2, 1, 31)

birthdays_original = {}


def manage_input():
    """
    Distinguish between positional argument and stdin.
    Call parse_input() with the respective parameter.
    """
    if len(sys.argv) is 1:
        parse_input(sys.stdin)
    elif len(sys.argv) is 2:
        with open(sys.argv[1]) as in_file:
            parse_input(in_file)
    else:
        print("Too many positional arguments!", file=sys.stderr)
        sys.exit(1)


def parse_input(file):
    """
    Get a file's values into the birthdays_original data structure.
    """
    for line in file:
        line_s = line.strip().split(',')
        birthdays_original[line_s[2]] = date(1, int(line_s[0]), int(line_s[1]))


def interpolate(birthdays):
    """
    Take a set of birthdays and map them between the globally specified 'start' and 'end'.
    :param birthdays: Dictionary of 'Name':date
    :return: A new dict of changed birthdays.
    """
    birthdays_adjusted = {}
    for key, value in birthdays.items():
        o = value.toordinal()

        n = ((o - 0) / (365 - 0)) * (end.toordinal() - start.toordinal()) + start.toordinal()

        birthdays_adjusted[key] = date.fromordinal(int(round(n)))

    return birthdays_adjusted


def display_birthdays(birthdays):
    print("Month\tDay\tName")
    for key, value in birthdays.items():
        print(f'{value.month}\t\t{value.day}\t{key}')


if __name__ == '__main__':
    manage_input()
    print("Original")
    display_birthdays(birthdays_original)
    print("Interpolated")
    display_birthdays(interpolate(birthdays_original))
