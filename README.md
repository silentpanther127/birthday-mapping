# Birthday Mapping

**Map a set of birthdays into a specified range.**

Me and my friends who live in the same house for this exchange semester would like to make birthday parties.
Our birthdays are not all during the time we are here though.
So we map a whole year into one semester.

## Idea

When the semester goes from 1.10. to 31.1. someone who's real birthday is on 1.1., should be on 1.10.
And so on.

## Input syntax

`Month,Day,Name`
```
10,13,Max
12,31,Jane
01,01,Tob
```

## Synopsis

Call with a file as specified above as a positional argument or pipe it as stdin.

## set the semester boundaries

In the `birthday-mapping.py` at the beginning there are variables called `start` and `end`.
They are of type `datetime.date` and follow `date(Year, Month, Day)`.
We use year 1 but a semester can go into a second year.